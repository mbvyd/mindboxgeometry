﻿using System;

namespace Geometry.Figures;

public class Triangle : FigureBase
{
    public double SideA { get; }
    public double SideB { get; }
    public double SideC { get; }

    public Triangle(double sideA, double sideB, double sideC)
    {
        SideA = sideA;
        SideB = sideB;
        SideC = sideC;

        if (!IsValid())
        {
            throw new Exception("Triangle is not valid.");
        }
    }

    public override double GetArea()
    {
        double semiPerimeter = (SideA + SideB + SideC) / 2;

        return Math.Sqrt(
            semiPerimeter *
            (semiPerimeter - SideA) *
            (semiPerimeter - SideB) *
            (semiPerimeter - SideC));
    }

    public bool IsRectangular()
    {
        bool isRectangular = false;

        double squareA = Math.Pow(SideA, 2);
        double squareB = Math.Pow(SideB, 2);
        double squareC = Math.Pow(SideC, 2);

        if ((squareA == squareB + squareC) ||
            (squareB == squareA + squareC) ||
            (squareC == squareA + squareB))
        {
            isRectangular = true;
        }

        return isRectangular;
    }

    private bool IsValid()
    {
        bool isValid = false;

        if ((SideA < SideB + SideC) &&
            (SideB < SideA + SideC) &&
            (SideC < SideA + SideB))
        {
            isValid = true;
        }

        return isValid;
    }
}
