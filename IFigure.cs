﻿namespace Geometry;

public interface IFigure
{
    public double GetArea();

    public int GetAreaRounded();
}
