﻿using System;

namespace Geometry;

public abstract class FigureBase : IFigure
{
    public abstract double GetArea();

    public int GetAreaRounded()
    {
        double area = GetArea();

        return (area % 1) != 0
            ? (int)Math.Round(area)
            : (int)area;
    }
}
